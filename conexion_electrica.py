#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import create_async_engine
from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime
import pytz
from rich import print
import click
from pathlib import Path
import ujson as json

def create_conn(data):
    url_db = "{dbengine}+{pgmodule}://{user}:{password}@{host}:{port}/postgres".format(**data)
    engine = create_engine(url_db, client_encoding='utf8')
    print("Conexion a ",engine)
    conn = engine.connect()
    return engine, conn

def run_engine(data):
    engine, conn = create_conn(data)
    tables = load_tables(conn)
    sel = ["valorizado", "valorizado_mensual", "valorizado_mensual_horaria"]
    sel_tables = [table for table in tables if table.name
                  in sel]
    table_columns = show_columns_table(conn, sel_tables)

    barras = show_barras(conn, sel_tables)
    """
    save to file
    """
    data = {
        "tablas": sel_tables,
        "columnas": table_columns,
        "barras": barras
    }
    valorizacion = Path("valorizacion.dat")
    with valorizacion.open("wb") as f:
        pickle.dump(data, f)
        print(valorizacion.stat().st_size)
    return engine, conn

@click.command()
@click.option("--datafile", default=Path("./datafile.json"), type=click.Path())
def run(datafile):
    txt = datafile.read_text()
    data = json.loads(txt)
    engine, conn = run_engine(data)
    conn.close()


@dataclass(frozen=True)
class Table: 
  name:str 

def load_tables(conn):
    query_tables = """
    SELECT table_name FROM information_schema.tables
                          WHERE table_schema='public';
    """
    query_tables = """
    SELECT table_name FROM information_schema.tables
                          WHERE table_schema='public';
    """
    tables = []
    rs = conn.execute(query_tables)
    print(rs)
    for row in rs:
        tables.append(Table(row[0]))
    return tables



@dataclass(frozen=True)
class ColumnType: 
  position:int 
  table_name:str
  column_name:str 
  data_type:str

  map_types = {
      "text":str,
      "int2":int,      
      "int4":int,
      "int8":int,
      "float8":float,
      "bool":bool,
      "timestamp":int,
      "float":float      
  }

  def __repr__(self):
    return f"{self.table_name}:{self.column_name}:{self.py_type}"

  @property
  def py_type(self):
      return self.map_types.get(self.data_type)


def show_columns_table(conn, tables):
    columns = {table:{} for table in tables}
    for table in tables:
        table_column = columns[table]
        column_types=f"""SELECT ordinal_position, table_name, column_name,  udt_name FROM information_schema.columns 
WHERE table_schema = 'public' AND table_name in (
SELECT table_name FROM information_schema.tables
                      WHERE table_schema='public' and table_name = '{table.name}'
)
ORDER BY (table_name, ordinal_position);
"""
        rs = conn.execute(column_types)
        for row in rs:
          col = ColumnType(*row)
          table_column[col.column_name] = col
    return columns

import re
from dataclasses import dataclass as std_dataclass, asdict

@dataclass
class BarraTabla:
    tabla:Table    
    name:str

    def __post_init__(self):
        try:
            sube_tension = re.sub("(_){1,}","#",self.name).split("#")
            tension = sube_tension[-1]
            sube = "_".join(sube_tension[0:-1])
            self.subestacion = sube
            self.tension = 0
            if tension.isdigit():
                self.tension = int(tension)
        except Exception as e:
            print("ERROR", e, self.name)

    @property
    def tension_str(self):
        return f"{int(self.tension)} [kV]"

    def dict(self):
        return asdict(self)

Barra = BarraTabla
  
def show_barras(conn, tables):
    barras = {table:[] for table in tables if table.name.startswith("valorizado")}
    for tabla in barras:
        barras_query = f"""
select distinct(nombre_barra) from {tabla.name} order by nombre_barra;
"""
        print(barras_query)
        rs = conn.execute(barras_query)
        for row in rs:
            data = (tabla, *row)
            barra = BarraTabla(*data)
            barras[tabla].append(barra)
    return 

import pickle
from pathlib import Path

if __name__== '__main__':
    run()

from pathlib import Path
import json
from rich import print
# https://apis.digital.gob.cl/fl
from pydantic.dataclasses import dataclass
from datetime import datetime
from dataclasses import field
from pydantic import BaseModel, validator
from typing import Optional
from datetime import date, timedelta

here = Path(__file__).parent

@dataclass(frozen=True)
class Feriado:
    nombre: str
    tipo: str 
    irrenunciable: bool
    fecha: datetime 
    comentarios:Optional[str]

    @validator("fecha", pre=True)
    def parse_fecha(cls, value):
        return datetime.strptime(value, "%Y-%m-%d")

    @validator("irrenunciable", pre=True)
    def parse_irrenunciable(cls, value):
        return value=="1"


def todos_los_domingos(year):
   d = date(year, 1, 1)                    # January 1st
   d += timedelta(days = 6 - d.weekday())  # First Sunday
   while d.year == year:
      yield d
      d += timedelta(days = 7)

def todos_los_sabados(year):
   d = date(year, 1, 1)                    # January 1st
   d += timedelta(days = 5 - d.weekday())  # First Sunday
   while d.year == year:
      yield d
      d += timedelta(days = 7)
    

      
def read_feriados():
    data = []
    years = set()
    for filejson in here.glob("feriados_*.json"):
        year = filejson.name.replace("feriados_","").replace(".json","")
        if year.isdigit():
            years.add(int(year))
        txt = filejson.read_text()
        data += json.loads(txt)
    domingos = set()
    for year in years:
        domingos_year = {d for d in todos_los_domingos(year)}
        domingos |= domingos_year
    sabados = set()
    for year in years:
        sabados_year = {d for d in todos_los_sabados(year)}
        sabados |= sabados_year
    
    dataset = set()
    for d in data:
        try:
            if "leyes" in d:
                del d["leyes"]
            f = Feriado(**d)
            dataset.add(f)
        except Exception as e:
            print(d, e)
            raise e
    return dataset, domingos, sabados



if __name__ == "__main__":
    feriados, domingos = read_feriados()
    print(feriados)

    FERIADOS, DOMINGOS = read_feriados()
    DATES_FERIADOS = {f.fecha for f in FERIADOS} | domingos

    print(len(DATES_FERIADOS))


from read_geodata import load_shapes_chile, plot_chile, save_shape_shp
from geopandas import GeoSeries


if __name__ == "__main__":
    chile, created = load_shapes_chile()
    print("Chile es es->",type(chile))
    print("Convex Hull", chile.convex_hull)
    print("Created", created)
    if created:
        save_shape_shp(chile)
    plot_chile(chile)

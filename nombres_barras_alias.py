
from read_barras_geodata import InfoBarra
from dataclasses import dataclass
from pathlib import Path
import pickle
from conexion_electrica import create_conn
import json
from typing import List, Dict, Any, Optional, Set

    
def get_value(key, conn, dataset):
    raw = f"""select \"Nombre Subestación\",\"Nombre Balances Transferencias\"
    from barras_aux as ba where ba.\"Nombre Balances Transferencias\" like  '%%{key}%%';
    """
    print("RRAWW")
    print(raw)
    rs = conn.execute(raw)
    row = (None,)
    for row in rs:
        print("Result", row)
        dataset[row[0]] = key
    if rs:
        print("Name barra-",row[0], key)
    else:
        print(key, "not found")

def obtain_info_barras(conn):
    barras_path = Path(__file__).parent / "info_barras_geo.pydat"
    barras = {}
    if not barras_path.exists():
        print("Check read_geodata.py")
    else:
        barras = pickle.loads(barras_path.read_bytes())
    return barras


def read_dataframe_stats():
    barras_path = Path(__file__).parent / "data_stats.pydat"
    barras = {}
    if not barras_path.exists():
        print("Check read_geodata.py")
    else:
        dataframe = pickle.loads(barras_path.read_bytes())
        return dataframe

def save_barras(
        barras:Dict[str, InfoBarra],
        barras_path:Path=Path(__file__).parent / "info_barras_name_geo.pydat"):
    data_bytes = pickle.dumps(barras)
    barras_path.write_bytes(data_bytes)
    return barras_path

def add_info_name(conn, barras):
    map_names = {}
    data_stats = read_dataframe_stats()
    for index, row in data_stats.iterrows():
        
        key = row["nombre"]       
        get_value(key, conn, map_names)
    save_barras(barras)
    return map_names

if __name__ == "__main__":
    datafile = Path(__file__).parent / "datafile.json"
    engine, conn = create_conn(json.loads(datafile.read_text()))
    barras = obtain_info_barras(conn)
    add_info_name(conn, barras)



    

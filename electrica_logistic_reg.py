from pathlib import Path
import pickle
from rich import print
import numpy as np
from logistic.logistic_regression import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split

def load_data():
    path = Path("data_stats.pydat")
    databytes = path.read_bytes()
    data = pickle.loads(databytes)
    return data

def save_data(data):
    path = Path("logistic_regression_electrica.pydat")
    databytes = pickle.loads(data)
    databytes = path.write_bytes(databytes)
    return path


data = load_data()

X = data[["media","dev_std", "analisis_value", "llave_value"]].to_numpy()

## clase para cada instancia anterior
y = data.tipo  


X_train, X_test, y_train, y_test = train_test_split(
        X, 
        y, 
        test_size=.60, 
        random_state=2,
        stratify=y)

regression = dict(
    eta=15,
    iterations=1000,
    do_history=True,
    refresh=True,
    clean=True,
    ok_clases=False,
    stop_condition=1e-5,
    data_name="electrica")
    
clf = LogisticRegression(**regression).fit(X_train, y_train, clean=True)

y_pred = clf.predict(X_test)


result = save_data(clf)
print(f"Result", result)




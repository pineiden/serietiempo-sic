
from read_barras_geodata import InfoBarra
from dataclasses import dataclass
from pathlib import Path
import pickle
from conexion_electrica import create_conn
import json
from typing import List, Dict, Any, Optional, Set
from rich import print
    

def obtain_info_barras():
    barras_path = Path(__file__).parent / "info_barras_geo.pydat"
    barras = {}
    if not barras_path.exists():
        print("Check read_geodata.py")
x    else:
        barras = pickle.loads(barras_path.read_bytes())
    return barras



if __name__ == "__main__":
    barras = obtain_info_barras()
    print(barras)



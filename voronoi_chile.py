import csv
import numpy as np
from rich import print
from pathlib import Path
from scipy.spatial import Voronoi, voronoi_plot_2d
import geopandas as gpd
from read_geodata import load_info_barras_geo
from read_geodata import load_shapes_chile, plot_chile, save_shape_shp
import pandas as pd
import pyproj
from shapely.ops import transform
from shapely.geometry import Point
import matplotlib.pyplot as plt
from shapely.geometry import Polygon
from shapely.ops import unary_union

def chile_convex_hull():
    chile, created = load_shapes_chile()
    chile_buffered = []
    print(len(chile.geometry.geoms))
    for g in chile.geometry.geoms:
        geom = g.buffer(1000000).exterior
        chile_buffered.append(geom)
    union = unary_union(chile_buffered)
    print("UNION->", len(union), type(union))
    print(chile.convex_hull, type(chile.convex_hull))
    chile_union = gpd.overlay(
        *chile.convex_hull,
        how='union')
    print("Union", chile_union)
    return chile, chile_union

def convert_deg_proj(lat,lon):
    wgs84 = pyproj.CRS('EPSG:4326')
    utm = pyproj.CRS('EPSG:32618')
    project = pyproj.Transformer.from_crs(
        wgs84,
        utm,
        always_xy=True).transform
    wgs84_pt = Point(lon, lat)
    utm_point = transform(project, wgs84_pt)
    return utm_point.x, utm_point.y

def read_data_comunas():
    dataset = {}
    csv_path = Path("chile_geo.csv")
    with csv_path.open() as f:
        reader = csv.DictReader(f,delimiter=';')
        for row in reader:
            lat = float(row["latitud"])
            lon = float(row["longitud"])
            x,y = convert_deg_proj(lat, lon)
            dataset[(x,y)] = {
                "id": int(row["cut"]),
                "comuna": row["nombre"],
                "latitud": lat,
                "longitud": lon,
                "superficie": float(row["superficie"]),
                "poblacion": float(row["poblacion"]),
                "densidad": float(row["densidad"]),
                "x": x,
                "y": y
            }

    df = pd.DataFrame(dataset.values())        
    return df, dataset


if __name__ == "__main__":
    chile, chile_convex = chile_convex_hull()
    #chile_convex.plot(ax=chile.plot())
    comunas, map_comunas = read_data_comunas()
    position = comunas[["x", "y"]]
    print("Posisiones", position)
    #position.plot.scatter(x="x", y="y")
    vor = Voronoi(position)
    print(vor)
    #plt.show()

    # hasta aqui convex_hull parece calzar con la serie de putnos de comunas
    
#barras = load_info_barras_geo()

# comunas = {}
# comuna = None
# for name, barra in barras.items():
#     if barra.geodata:
#         data_comuna = dataset.get(barra.geodata.cut_comuna)
#         barra.geodata.set_poblacion(data_comuna.get("poblacion"))
#         barra.geodata.set_densidad(data_comuna.get("densidad"))
#         if barra.geodata.cut_comuna not in comunas:
#             comuna = barra.geodata.dict()
#             comunas[barra.geodata.cut_comuna] = comuna
#     else:
#         print("Barra sin comuna", barra)


# print(barras)

        
# df = pd.DataFrame(data=list(comunas.values()), columns=list(comuna.keys()))

# print(comunas)
# # print(df)

# gdf = gpd.GeoDataFrame(
#     df,
#     geometry=df.comuna_geo
# )
# gdf = gdf.set_crs('epsg:32618')

# print(gdf)
# import geopandas

# world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))

# # We restrict to South America.
# ax = world[world.name == 'Chile'].plot(
#     color='white', edgecolor='black')

# import matplotlib.pyplot as plt

# #gdf.plot(ax=ax, color='red')

# #plt.show()

# points = [(comuna["center"].x,comuna["center"].y)  for comuna in comunas.values()]

# print(points)
# vor = Voronoi(points)
# print(vor.regions)


# # """

# # Voronoi presenta una solucion regional de distribución equitativa a cada punto
# # específico.
# # https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.Voronoi.html
# # https://github.com/WZBSocialScienceCenter/geovoronoi
# # """
# import re 
# from geovoronoi import voronoi_regions_from_coords

# world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
# # #print("CHECKKK", re.search("Chile",  world.name))
# # #print(world[search("Chile",  world.name)])
# # #
# area = world[world.name == 'Chile']

# # print("Area Chile", area)

# area = area.to_crs(epsg=32618)
# # convert to World Mercator CRS
# area_shape = area.iloc[0].geometry 
# dataset = []
# # # for i, point in enumerate(gdf["geometry"]):
# # #     print(point.within(area_shape))
# # #     if area_shape.contains(point):
# # #         pass
# # #     else:
# # #         print(gdf["comuna"][i], point,area_shape.contains(point))

# # print("DATSET", dataset)        
# continente = gdf[gdf["geometry"].within(area_shape)]
# continente = gpd.overlay(area, gdf, how='intersection')


# # print("COntinente OK")
# # print(continente)

# total_points = gdf["geometry"]
# # points = continente["geometry"]

# region_polys, region_pts = voronoi_regions_from_coords(total_points, area_shape)
# from geovoronoi.plotting import subplot_for_map, plot_voronoi_polys_with_points_in_area
# # fig, ax = subplot_for_map()
# plot_voronoi_polys_with_points_in_area(ax, area_shape, region_polys, total_points, region_pts)
# plt.show()

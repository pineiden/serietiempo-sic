from pathlib import Path
import fiona
from rich import print
import re
from pydantic.dataclasses import dataclass
import shapely
from conexion_electrica import create_conn
from pathlib import Path
import json
from rich import print
import pickle
import time
from shapely.ops import transform
from shapely.ops import unary_union
import geopandas as gpd
#pdte cartopyimport geoplot
from matplotlib import pyplot as plt
from shapely.geometry import mapping, shape

from typing import List, Dict, Any, Optional, Set
from dataclasses import field
from shapely.geometry import Point
from shapely.geometry import shape, Polygon, MultiPolygon

from read_barras_geodata import (obtain_info_barras, InfoBarra,
                                 GeoInfo, project_chile)

import pyproj

"""
Put every station inside their 'comuna'
"""
basepath = Path(__file__).parent / "geodata"


def load_shapes_intersect_barras(barras, read="COMUNAS"):
    barras_descarte = set(barras.keys())
    for filename in basepath.rglob("*.shp"):
        print(filename.name)
        if read in filename.name or read=='ALL':
            with fiona.open(filename) as src:
                meta = src.meta
                data_props = meta["schema"]["properties"]
                # read information from every comuna
                for comuna in src:
                    data = dict(
                        region=comuna["properties"]["REGION"],
                        cut_region=comuna["properties"]["CUT_REG"],
                        provincia=comuna["properties"]["PROVINCIA"],
                        cut_provincia=comuna["properties"]["CUT_PROV"],
                        comuna=comuna["properties"]["COMUNA"],
                        cut_comuna=comuna["properties"]["CUT_COM"],
                        superficie=float(comuna["properties"]["SUPERFICIE"]))
                    poligonos = transform(project_chile,
                                          shape(comuna["geometry"]))
                    data["comuna_geo"] = poligonos
                    gib = GeoInfo(**data)
                    mover = set()
                    for barra_key in barras_descarte:
                        barra = barras[barra_key]
                        if barra.point.within(poligonos):
                            barra.set_geodata(gib)
                            mover.add(barra_key)
                    [barras_descarte.remove(b) for b in  mover]
    return barras


def save_shape_shp(
        shape,
        filepath=Path(__file__).parent / "geodata" / "chile-limits",
        limite=0.005):

    # schema = {
    #     'geometry':'MultiPolygon',
    #     'properties':[('Name','str')]
    # }
    try:
        areas = sorted([e.area for e in list(shape.geoms)])
        limit = max(areas) * limite
        # toma las principales areas de los territorios, descarta islas pequeñas
        valid_shapes = [p for p in list(shape) if p.area >= limit]
        gdf3=gpd.GeoDataFrame(geometry=valid_shapes)
        gdf3.to_file(filename=str(filepath), driver='ESRI Shapefile')

        # with fiona.open(
        #         filepath,"w",
        #         driver="ESRI Shapefile",
        #         schema=schema,
        #         crs="EPSG:32618") as output:
        #     diccionario = {
        #         "properties":{"Name":"Chile"},
        #         "geometry": [mapping(e) for e in valid_shapes]
        #     }
        #     output.write(diccionario)
    except Exception as e:
        raise e

def load_shapes_chile( read="REGIONES"):
    dataset = []
    chile_path = Path(__file__).parent / "geodata" / "chile-limits"
    chile_path.parent.mkdir(parents=True, exist_ok=True)
    chile = None
    if not chile_path.exists():
        for filename in basepath.rglob("*.shp"):
            if read in filename.name or read=='ALL':
                with fiona.open(filename) as src:
                    meta = src.meta
                    data_props = meta["schema"]["properties"]
                    for elem in src:
                        p = elem["properties"]
                        print(p)
                        geo = transform(project_chile,
                                              shape(elem["geometry"]))

                        dataset.append(geo)

        chile = unary_union(dataset)
        #union of poligonos =  limites de chile
        # data_bytes = pickle.dumps(chile)
        # chile_path.write_bytes(data_bytes)
        print("Union of polygons ok!...saving file")
        return chile, True
    else:
        chile = gpd.read_file(chile_path)
        print("Load chile--> polygon multi geopandas", chile)
        return chile, False

def plot_chile(chile):
    chile.plot(color='blue')
    plt.show()

def save_barras(
        barras:Dict[str, InfoBarra],
        barras_path:Path=Path(__file__).parent / "info_barras_geo.pydat"):
    data_bytes = pickle.dumps(barras)
    barras_path.write_bytes(data_bytes)
    return barras_path

def load_info_barras_geo():
    barras_path = Path(__file__).parent / "info_barras_geo.pydat"
    data_bytes = barras_path.read_bytes()
    data_barras = pickle.loads(data_bytes)
    return data_barras

if __name__ == "__main__":
    datafile = Path(__file__).parent / "datafile.json"
    engine, conn = create_conn(json.loads(datafile.read_text()))
    barras = obtain_info_barras(conn)
    barras_comuna = load_shapes_intersect_barras(barras)
    barras_path = save_barras(barras_comuna)
    print(f"Data saved to {barras_path}")


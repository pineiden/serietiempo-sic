from pathlib import Path
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from rich import print
import pandas as pd
# IMPORTAR DESDE EL ARCHIVO QUE CORRESPONDA
from logistic_regression import LogisticRegression, MatrixNxD, MatrixNx1_C
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import multiprocessing
from typing import Dict, List
from multiprocessing import Pool, Manager
import asyncio
import concurrent.futures as cf
from functools import partial

def plot(dataset_lr, pathname):
    sns.set()
    fig, (ax1,ax2,ax3,ax4) = plt.subplots(4, 1, sharex=True,)
    elem = dataset_lr[0]
    sns.set_theme(style="darkgrid")
    # Load an example dataset with long-form data
    fmri = pd.DataFrame([lr.dict() for lr in dataset_lr], 
                        columns=["iteration","value","error_ll","error_slope","duration"])
    # Plot the responses for different events and regions

    fmri['value'] = fmri['value'].astype(float)
    fmri['error_ll'] = fmri['error_ll'].astype(float)
    fmri['error_slope'] = fmri['error_slope'].astype(float)
    fmri['duration'] = fmri['duration'].astype(float)

    ax1.set_title("Log-Verosimilitud", fontsize=10)
    ax2.set_title("Error e=(LL1-LL0)", fontsize=10)
    ax3.set_title("Pendiente del error delta(e)", fontsize=10)
    ax4.set_title("Duración de iteración", fontsize=10)

    plot1 = sns.lineplot(
        x="iteration",
        y="value", 
        data=fmri, ax=ax1)
    plot1.set_xlabel("Iteración", fontsize=8)
    plot1.set_ylabel("Log-Verosimilitud", fontsize=8)

    plot2 = sns.lineplot(
        x="iteration",
        y="error_ll", 
        color='red',
        data=fmri, ax=ax2)
    plot2.set_xlabel("Iteración", fontsize=8)
    plot2.set_ylabel("Error LL", fontsize=8)

    plot3 = sns.lineplot(
        x="iteration",
        y="error_slope", 
        color='orange',
        data=fmri, ax=ax3)
    plot3.set_xlabel("Iteración", fontsize=8)
    plot3.set_ylabel("Pendiente Error LL", fontsize=8)

    plot4 = sns.scatterplot(
        x="iteration",
        y="duration", 
        color='green',
        data=fmri, ax=ax4)
    plot4.set_xlabel("Iteración", fontsize=8)
    plot4.set_ylabel("Duración [s]", fontsize=8)

    fig.suptitle(f"Tasa  {elem.eta}", fontsize=12)
    print(f"Saving plot to {pathname}")
    fig.tight_layout()
    plt.savefig(str(pathname).replace("pydat","svg"))
    #fig = plot.get_figure()
    #fig.savefig(f"{pathname.replace('.pydat','')}.png") 


def run_regression(data, X, y, paths):
    # AQUI CADA REGRESION LOGISTICA PARTICULAR:
    regression = LogisticRegression(**data)
    print("Iniciando", regression)
    clf = regression.fit(X,y)
    path = clf.save()
    paths.append(path)
    plot(clf.history, path)

def run_regression_group(group,X,y, paths):
    for data in group:
        run_regression(data,X,y, paths)

from datetime import datetime

def run_parallel(data, amount, iterations, stop_condition):
    dataset = pd.read_csv(data)
    X = dataset[["deita","saiens"]].to_numpy()
    y = dataset["target"].to_numpy()
    workers = max(multiprocessing.cpu_count()-1, 2)
    processes:Dict[int,List[LogisticRegression]] = {
        i:[] for i in range(workers)}
    numbers = set(processes.keys())
    # ESTABLECER RANGO DE ITERACION Y CANTIDAD DE ELEMENTOS
    for eta in np.linspace(.001, 1, amount):
        key = numbers.pop()
        # CONSTRUIR DICCIONARIO CON LOS ARGUMENTOS DE TU REGRESSION
        regression = dict(
            eta=eta,
            iterations=iterations,
            do_history=True,
            refresh=True,
            stop_condition=stop_condition,
            data_name="abstract")
        processes[key].append(regression)
        if not numbers:
            numbers = set(processes.keys())


    with cf.ProcessPoolExecutor(workers) as executor:
        manager = Manager()
        paths = manager.list()
        loop = asyncio.get_event_loop()
        tasks = []
        for i, group in processes.items():
            if group:
                task = loop.run_in_executor(executor,
                                            partial(
                                                run_regression_group,
                                                group,X,y, paths))
                tasks.append(task)
        #loop.run_until_complete(asyncio.gather(*tasks))
        loop.run_until_complete(
            asyncio.gather(
                *tasks
            )
        )

        ts = datetime.utcnow().isoformat()
        paths_file = Path(__file__).parent / f"model-paths_{ts}.pydat"
        paths_file.write_bytes(pickle.dumps(paths))

if __name__ == '__main__':
    here = Path(".").parent
    data = here / "documentos/abstract_data.csv"
    amount = int(sys.argv[1])
    iterations = int(sys.argv[2])
    stop_condition = float(sys.argv[3])
    run_parallel(data, amount, iterations, stop_condition)

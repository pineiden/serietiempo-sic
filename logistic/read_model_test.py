from datetime import datetime
from pathlib import Path
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from rich import print
import pandas as pd
# IMPORTAR DESDE EL ARCHIVO QUE CORRESPONDA
from logistic_regression import (LogisticRegression, MatrixNxD,
                                 MatrixNx1_C, softmax)
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import multiprocessing
from typing import Dict, List
from multiprocessing import Pool, Manager
import asyncio
import concurrent.futures as cf
from functools import partial
from scoring import (
    Montecarlo, 
    TotalMontecarlo, 
    Scoring, 
    mc_parallel, 
    run_group, 
    run_queue_montecarlo)
import psutil 
import sys
from sklearn.preprocessing import PolynomialFeatures
import click
import seaborn as sns
import itertools

def generate_poly_x(X):
        poly1 = PolynomialFeatures(1)
        poly3 = PolynomialFeatures(3)
        Xpol1 = np.delete(
            poly1.fit_transform(X),
            0,
            axis=1)
        Xpol3 = np.delete(
            poly3.fit_transform(X),
            0,
            axis=1)
        return Xpol1, Xpol3

def read_data(datapath:Path):
    if datapath.exists():
        dataset = pd.read_csv(datapath)
        X = dataset[["deita","saiens"]].to_numpy()
        y = dataset["target"].to_numpy()
        Xpol1,Xpol3 = generate_poly_x(X)
        return (X, Xpol1, Xpol3), y
    else:
        print(f"Archivo de datos {datapath} no existe")


def read_models(modelpath, model_p2, model_p3):
    pass


def load_dataset(pathname:Path):
    try:
        databytes = pathname.read_bytes()
        dataset = pickle.loads(databytes)
        return dataset
    except Exception as e:
        raise e


def plot_dataset(pathname:Path):
    try:
        dataset = load_dataset(pathname).LL_list
        dt = datetime.utcnow().isoformat()
        plot_path = HERE / f"img/model-logistic-reg-{dt}.svg"
        plot = plot_model(dataset, plot_path)
    except Exception as e:
        raise e



def plot_sim(fmri, pathname, name, show=True):
    sns.set()
    fig, (ax1,ax2,ax3,) = plt.subplots(3, 1, sharex=True,)
    sns.set_theme(style="darkgrid")
    # Load an example dataset with long-form data
    # Plot the responses for different events and regions

    fmri['aic'] = fmri['aic'].astype(float)
    fmri['bic'] = fmri['bic'].astype(float)
    fmri['acc'] = fmri['acc'].astype(float) * 100

    ax1.set_title("AIC", fontsize=10)
    ax2.set_title("BIC", fontsize=10)
    ax3.set_title("Cross Validation", fontsize=10)

    plot1 = sns.lineplot(
        x="iter",
        y="aic", 
        data=fmri, ax=ax1)
    plot1.set_xlabel("Iteración", fontsize=8)
    plot1.set_ylabel("AIC", fontsize=8)

    plot2 = sns.lineplot(
        x="iter",
        y="bic", 
        color='red',
        data=fmri, ax=ax2)
    plot2.set_xlabel("Iteración", fontsize=8)
    plot2.set_ylabel("BIC", fontsize=8)

    plot3 = sns.lineplot(
        x="iter",
        y="acc", 
        color='orange',
        data=fmri, ax=ax3)
    plot3.set_xlabel("Iteración", fontsize=8)
    plot3.set_ylabel("ACC[%]", fontsize=8)


    fig.suptitle(f"Simulacion Montecarlo '{name}'", fontsize=12)
    print(f"Saving plot to {pathname}")
    fig.tight_layout()
    plt.savefig(str(pathname).replace("pydat","svg"))
    #fig = plot.get_figure()
    #fig.savefig(f"{pathname.replace('.pydat','')}.png") 
    if show:
        plt.show()


def better_models(datasets):
    field = "acc"
    models = {k:max(v, key=lambda e:e[field]) for k,v in datasets.items()}
    return models


def save_models(datapath:Path, models):
    databytes = pickle.dumps(models)
    datapath.write_bytes(databytes)
    print(f"Models saved at {datapath}")



def ponderacion(sim_montecarlo):
    here = Path(__file__).parent 
    data = dict(
        eta=.01,
        iterations=1,
        do_history=True,
        refresh=False,
        stop_condition=1e-5,
        data_name="montecarlo")
    iters = 100
    val = .4
    results = {k:{
            "aic":{delta:LogisticRegression(**data) for delta in np.linspace(-val,val,iters)},
            "bic":{delta:LogisticRegression(**data) for delta in np.linspace(-val,val,iters)}
    } for k in sim_montecarlo}
    for k,v in sim_montecarlo.items():
        for delta in np.linspace(-val,val,iters):
            scoring_aic = softmax(-(.5+delta)*np.array([float(e.score.aic) for e in v]))
            scoring_bic = softmax(-(.5+delta)*np.array([float(e.score.aic) for e in v]))
            pond_w_aic = sum([v[i].model.W*s for i,s in enumerate(scoring_aic)])
            pond_w_bic = sum([v[i].model.W*s for i,s in enumerate(scoring_bic)])
            model_aic = results[k]["aic"][delta]
            model_bic = results[k]["bic"][delta]
            model = v[-1].model
            model_aic.set_params(pond_w_aic, model.y_clases,
                                model.ok_clases, model.bit_to_class)
            model_bic.set_params(pond_w_bic, model.y_clases,
                                model.ok_clases, model.bit_to_class)

    # creating the model
    print(results)
    save_models(here/"montecarlo-models-ponderado.pydat", results)

@click.command()
@click.option(
    "--model", 
    type=click.Path(exists=True), 
    help="modelpath")
def run(model):
    here = Path(__file__).parent 
    modelpath = Path(model)
    #run_parallel(datapath, iterations, stop_condition)
    if modelpath.exists():    
        total_montecarlo = load_dataset(modelpath)
        sim_montecarlo = total_montecarlo.grouped()
        datasets = {k:[item.data() for item in v]  for k,v in sim_montecarlo.items()}
        df_p1 = pd.DataFrame(datasets["lineal"])
        df_p3 = pd.DataFrame(datasets["poly3"])
        plot_sim(df_p1, here /
                 "img/montecarlo_lineal_2000.svg","Lineal", show=False)
        plot_sim(df_p3, here /
                 "img/montecarlo_poly3_2000.svg","Polinomio(°3)",
                 show=False)
        seleccion = better_models(datasets)
        models = {k:[m for m in sim_montecarlo[k] if
                     m.niter==v["iter"]] for k,v in seleccion.items()}
        save_models(here/"montecarlo-selection.pydat", models)
        ponderacion(sim_montecarlo)
    else:
        print("Path to model {modelpath} does not exists")


if __name__ == "__main__":
    run()

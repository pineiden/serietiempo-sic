from datetime import datetime
from pathlib import Path
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from rich import print
import pandas as pd
# IMPORTAR DESDE EL ARCHIVO QUE CORRESPONDA
from logistic_regression import LogisticRegression, MatrixNxD, MatrixNx1_C
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import multiprocessing
from typing import Dict, List
from multiprocessing import Pool, Manager
import asyncio
import concurrent.futures as cf
from functools import partial
from scoring import (
    Montecarlo, 
    TotalMontecarlo, 
    Scoring, 
    mc_parallel, 
    run_group, 
    run_queue_montecarlo)
import psutil 
import sys

def run_parallel(data, iterations, stop_condition):
    dataset = pd.read_csv(data)
    X = dataset[["deita","saiens"]].to_numpy()
    y = dataset["target"].to_numpy()
    workers = max(psutil.cpu_count(logical = False)-2, 2)
    # ESTABLECER RANGO DE ITERACION Y CANTIDAD DE ELEMENTOS
    y_clases = set([elem for elem in y])
    processes = mc_parallel(
        X,
        y,
        y_clases,
        workers,
        iters=iterations,
        start=.15,
        end=.85)
    nums = len([key for key, v in processes.items() if len(v)>=1])
    with cf.ProcessPoolExecutor(workers+1) as executor:
        manager = Manager()
        queue  = manager.Queue()
        loop = asyncio.get_event_loop()
        tasks = []
        for n, group in processes.items():
            if group:
                task = loop.run_in_executor(executor,
                                            partial(
                                                run_group,
                                                n,
                                                group,
                                                queue))
                tasks.append(task)

        task = loop.run_in_executor(executor, partial(
            run_queue_montecarlo, queue, nums))
        tasks.append(task)

        #loop.run_until_complete(asyncio.gather(*tasks))
        loop.run_until_complete(
            asyncio.gather(
                *tasks
            )
        )

        


if __name__ == '__main__':
    main = Path(__file__).parent.parent
    datapath = main / "documentos/abstract_data.csv"
    iterations = int(sys.argv[1])
    stop_condition = float(sys.argv[2])
    run_parallel(datapath, iterations, stop_condition)

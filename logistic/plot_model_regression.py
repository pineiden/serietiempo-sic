from pathlib import Path
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import click
from rich import print
from datetime import datetime
import pandas as pd

HERE = Path(__file__).parent

def plot_model(dataset_lr, pathname):
    sns.set()
    fig, (ax1,ax2,ax3,ax4) = plt.subplots(4, 1, sharex=True,)
    elem = dataset_lr[0]
    sns.set_theme(style="darkgrid")
    # Load an example dataset with long-form data
    fmri = pd.DataFrame([lr.dict() for lr in dataset_lr], 
                        columns=["iteration","value","error_ll","error_slope","duration"])
    # Plot the responses for different events and regions

    fmri['value'] = fmri['value'].astype(float)
    fmri['error_ll'] = fmri['error_ll'].astype(float)
    fmri['error_slope'] = fmri['error_slope'].astype(float)
    fmri['duration'] = fmri['duration'].astype(float)

    ax1.set_title("Log-Verosimilitud", fontsize=10)
    ax2.set_title("Error e=(LL1-LL0)", fontsize=10)
    ax3.set_title("Pendiente del error delta(e)", fontsize=10)
    ax4.set_title("Duración de iteración", fontsize=10)

    plot1 = sns.lineplot(
        x="iteration",
        y="value", 
        data=fmri, ax=ax1)
    plot1.set_xlabel("Iteración", fontsize=8)
    plot1.set_ylabel("Log-Verosimilitud", fontsize=8)

    plot2 = sns.lineplot(
        x="iteration",
        y="error_ll", 
        color='red',
        data=fmri, ax=ax2)
    plot2.set_xlabel("Iteración", fontsize=8)
    plot2.set_ylabel("Error LL", fontsize=8)

    plot3 = sns.lineplot(
        x="iteration",
        y="error_slope", 
        color='orange',
        data=fmri, ax=ax3)
    plot3.set_xlabel("Iteración", fontsize=8)
    plot3.set_ylabel("Pendiente Error LL", fontsize=8)

    plot4 = sns.scatterplot(
        x="iteration",
        y="duration", 
        color='green',
        data=fmri, ax=ax4)
    plot4.set_xlabel("Iteración", fontsize=8)
    plot4.set_ylabel("Duración [s]", fontsize=8)

    fig.suptitle(f"Tasa  {elem.eta}", fontsize=12)
    print(f"Saving plot to {pathname}")
    fig.tight_layout()
    plt.savefig(str(pathname).replace("pydat","svg"))
    #fig = plot.get_figure()
    #fig.savefig(f"{pathname.replace('.pydat','')}.png") 
    plt.show()

def load_dataset(pathname:Path):
    try:
        print(f"Leyendo {pathname}")
        databytes = pathname.read_bytes()
        dataset = pickle.loads(databytes)
        return dataset
    except Exception as e:
        raise e


def plot_dataset(pathname:Path):
    try:
        dataset = load_dataset(pathname).LL_list
        dt = datetime.utcnow().isoformat()
        plot_path = HERE / f"img/model-logistic-reg-{dt}.svg"
        plot = plot_model(dataset, plot_path)
    except Exception as e:
        raise e

@click.command()
@click.option("--path", type=click.Path(exists=True), help="ruta de dataset LogLikehood")
def run(path):
    plot_dataset(Path(path))
    

if __name__ == "__main__":
    run()

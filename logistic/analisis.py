from pathlib import Path
from datetime import datetime
from pathlib import Path
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from rich import print
import pandas as pd
# IMPORTAR DESDE EL ARCHIVO QUE CORRESPONDA
from logistic_regression import LogisticRegression, MatrixNxD, MatrixNx1_C
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import multiprocessing
from typing import Dict, List
from multiprocessing import Pool, Manager
import asyncio
import concurrent.futures as cf
from functools import partial
from scoring import (
    Montecarlo, 
    TotalMontecarlo, 
    Scoring, 
    mc_parallel, 
    run_group, 
    run_queue_montecarlo)

import seaborn as sns
import matplotlib.pyplot as plt


def load_montecarlo(pathname):
    databytes = pathname.read_bytes()
    total_montecarlo = pickle.loads(databytes)
    return total_montecarlo


if __name__ == '__main__':
    sns.set()
    here = Path(__file__).parent
    pathname = here / "montecarlo-2000-2022-05-30T02:31:08.040874.pydat"
    total_montecarlo = load_montecarlo(pathname)
    montecarlo_last = total_montecarlo.valores[-1]
    dataset = [item.data() for item in iter(total_montecarlo)]
    dataset.sort(key=lambda e:e["split"])
    df = pd.DataFrame(dataset,
                      columns=["split","name","aic","bic","acc"])
    df_poly2 = df[df["name"]=="poly2"]
    df_poly3 = df[df["name"]=="poly3"]

    print(df_poly2.head())
    print(df_poly3.head())

    #df_poly2.plot(title="Poly2")

    df_poly2.plot(y=["aic","bic","acc"], x="split", title="Poly2",
                  subplots=True)
    plt.savefig(here / "img/montecarlo-1000-poly2.png")

    #plt.show()

    #df_poly3.plot(title="Poly3")
    df_poly3.plot(y=["aic","bic","acc"], x="split", title="Poly3",
                  subplots=True)
    plt.savefig(here / "img/montecarlo-1000-poly3.png")

    plt.savefig(str(pathname).replace("pydat","svg"))

    #plt.show()

    seleccion = df_poly3[df_poly3["split"].between(.5,.6)]
    acc = max(seleccion["acc"])
    print(acc)
    model = [v.model  for v in total_montecarlo.valores if v.score.acc==acc][-1]
    print("Model",model)

"""
Criterios de comparación
========================

AIC o Akaike:  
Usa todo el dataset


BIC o Bayesiano: 
Usa todo el dataset

MC o Montecarlo:
Usa train y test conjuntos.


Usar funcions: 

- accuracy_score de sklearn para calcular el 'accuracy' promedio

"""
from logistic_regression import LogisticRegression, MatrixNxD, MatrixNx1_C
from mpmath import mp as math
from sklearn.model_selection import train_test_split
import random
import pickle
from dataclasses import dataclass, field
from typing import List
import copy
from sklearn.preprocessing import PolynomialFeatures
import numpy as np
import asyncio
from rich import print
from tasktools.taskloop import TaskLoop
from datetime import datetime
from pathlib import Path
from functools import partial
from dataclasses import asdict

HERE = Path(__file__).parent


def generate_poly_x(X):
        poly1 = PolynomialFeatures(1)
        poly3 = PolynomialFeatures(3)
        Xpol1 = np.delete(
            poly1.fit_transform(X),
            0,
            axis=1)
        Xpol3 = np.delete(
            poly3.fit_transform(X),
            0,
            axis=1)
        return Xpol1, Xpol3


@dataclass
class Scoring:
    aic: float 
    bic: float
    mc: float

    def dict(self):
        return {
            "aic":float(self.aic),
            "bic":float(self.bic),
            "acc":float(self.acc)
        }

    @property
    def acc(self):
        return self.mc


@dataclass
class Montecarlo:
    name:str
    niter: int
    split: float
    model: LogisticRegression
    score: Scoring

    def dict(self):
        return asdict(self)

    def data(self):
        return {"name":self.name, 
                "split":self.split, 
                "iter":self.niter,
                **self.score.dict()}

import itertools

@dataclass
class TotalMontecarlo:
    valores: List[Montecarlo] = field(default_factory=list)

    def append(self,val:Montecarlo):
        self.valores.append(val)

    def final(self):
        scores = [v.score for v in self.valores]
        if scores:
            return sum(scores)/len(scores)
        else:
            return 0

    def dict(self):
        return asdict(self)

    @property
    def length(self):
        return len(self.valores)

    def __iter__(self):
        return iter(self.valores)

    def sort(self):
        self.valores.sort(key=lambda e: e.name)

    def grouped(self):
        sim_montecarlo = {}
        self.sort()
        for key, group in itertools.groupby(
                self.valores, lambda e:e.name):
            sim_montecarlo[key] = sorted(group, key=lambda e:e.niter)
        return sim_montecarlo

def mc_parallel(
        X,
        y,
        y_clases,
        workers,
        start=.5,
        end=.9,
        iters=1000):
    """
    Iteración:
    - Dividir train/test aleatorio
    - hacer fit de train, calcular el error 
    - repetir muchas veces
    - tomar promedio del error
    """

    processes = {i:[] for i in range(workers)}
    numbers = list(set(processes.keys()))
    #for split in np.linspace(start, end, iters):
    split = 0.56
    for niter in range(iters):
        k = numbers.pop(0)
        X_train, X_test, y_train, y_test = train_test_split(
            X,
            y,
            test_size=split)
        processes[k].append({
            "clases": y_clases,
            "split":split,
            "iter":niter,
            "train":{
                "X":X_train,"y":y_train
            },
            "test": {
                "X":X_test, "y":y_test
            }
        })
        if not numbers:
            numbers = list(set(processes.keys()))


    # return TotalMontecarlo, to get the split value more similar to
    # the main result
    return processes


def do_regression(
                niter,
                X, 
                y, 
                X_test, 
                y_test, 
                y_clases, 
                split, 
                name, 
                queue):
    data = dict(
        eta=.01,
        iterations=2000,
        do_history=True,
        refresh=True,
        stop_condition=1e-5,
        data_name="abstract")
    regression =  LogisticRegression(**data)
    regression.set_clases(y_clases)
    clf = regression.fit(X, y, clean=True)
    y_pred = regression.predict(X_test)
    BIC = clf.bic
    AIC = clf.aic
    ACC = regression.score(y_test, y_pred)
    score = Scoring(BIC, AIC, ACC)
    mc = Montecarlo(name, niter, split,  regression,  score)
    queue.put(mc)
    return score

def create_model(n, i, item, queue):
    X_train =  item["train"]["X"]
    X_test =  item["test"]["X"]
    X_train_p1, X_train_p3 = generate_poly_x(X_train)
    X_test_p1, X_test_p3 = generate_poly_x(X_test)
    y_train =  item["train"]["y"]
    y_test =  item["test"]["y"]
    split = item["split"]
    y_clases = item["clases"]
    niter = item["iter"]
    score = do_regression(
        niter,
        X_train_p1,
        y_train, 
        X_test_p1, 
        y_test, y_clases,
        split,
        name="lineal", queue=queue)
    print(f"Model ok -> {i}/{n}", 
          "lineal", 
          f"split {split}",score)

    score = do_regression(
        niter,
        X_train_p3, 
        y_train, 
        X_test_p3, 
        y_test, y_clases,
        split,
        name="poly3", queue=queue)
    print(f"Model ok -> {i}/{n}",
          "poly3", 
          f"split {split}",score)

def run_group(n, group, queue):
    for i,item in enumerate(group):
        print(f"Iniciando {i}/{n}")
        create_model(n, i, item, queue)
    print("Sending END to colector")
    queue.put("END")
        

async def read_queue_montecarlo(
        registro, queue, nums, 
        ends, task_name, **kwargs):
    """
    To run after run montecarlo
    """
    if len(ends)<nums:
        for i in range(queue.qsize()):
            item = queue.get()
            if isinstance(item, Montecarlo):
                registro.append(item)
            elif item=="END":
                ends.append(item)
            else:
                print("Wrong item", item)
            queue.task_done()

    else:
        dt = datetime.utcnow().isoformat()
        n = registro.length
        pathname = HERE / f"montecarlo-{n}-{dt}.pydat"
        databytes = pickle.dumps(registro)
        pathname.write_bytes(databytes)
        print("Simulación montecarlo guardada en", pathname)
        """
        END THIS TASKS
        """
        task = asyncio.current_task()
        task.add_done_callback(partial(
            print, "Collect Montecarlo END"))
        task.cancel()

    await asyncio.sleep(5)
    return (registro, queue, nums, ends, task_name), {}


def run_queue_montecarlo(queue, nums):
    loop = asyncio.get_event_loop()
    ends = []
    name = "queue_montecarlo"
    registro = TotalMontecarlo()
    try:
        task = TaskLoop(
            read_queue_montecarlo, 
            [registro,queue, nums, ends,name], {}, 
            name=name)
        task.create()
        if not loop.is_running():
            loop.run_forever()
    except asyncio.CancelledError as ce:
        print("Se termine proceso montecarlo")

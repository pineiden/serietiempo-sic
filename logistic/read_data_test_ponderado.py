from datetime import datetime
from pathlib import Path
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from rich import print
import pandas as pd
# IMPORTAR DESDE EL ARCHIVO QUE CORRESPONDA
from logistic_regression import LogisticRegression, MatrixNxD, MatrixNx1_C
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import multiprocessing
from typing import Dict, List
from multiprocessing import Pool, Manager
import asyncio
import concurrent.futures as cf
from functools import partial
from scoring import (
    Montecarlo, 
    TotalMontecarlo, 
    Scoring, 
    mc_parallel, 
    run_group, 
    run_queue_montecarlo)
import psutil 
import sys
from sklearn.preprocessing import PolynomialFeatures
import click
import seaborn as sns
import itertools

def generate_poly_x(X):
    # cambiar 2->1
    poly1 = PolynomialFeatures(1)
    poly3 = PolynomialFeatures(3)
    Xpol1 = np.delete(
        poly1.fit_transform(X),
        0,
        axis=1)
    Xpol3 = np.delete(
        poly3.fit_transform(X),
        0,
        axis=1)
    return Xpol1, Xpol3


def read_data(datapath:Path):
    if datapath.exists():
        dataset = pd.read_csv(datapath)
        X = dataset[["deita","saiens"]].to_numpy()
        y = dataset["target"].to_numpy()
        Xpol1,Xpol3 = generate_poly_x(X)
        print("Xshapes",Xpol1.shape, Xpol3.shape)
        return (X,Xpol1,Xpol3), y
    else:
        print(f"Archivo de datos {datapath} no existe")



def load_dataset(pathname:Path):
    try:
        databytes = pathname.read_bytes()
        dataset = pickle.loads(databytes)
        return dataset
    except Exception as e:
        raise e


def puntaje(model,X, y):
    y_pred = model.predict(X)
    score = model.score(y, y_pred)
    LL = model.log_likelihood(X,y)
    return score, LL, y_pred


def plot_data(name, model, X, Xp, y):
    """
    X :: direct data
    """
    sns.set()
    here = Path(__file__).parent

    Xs = Xp[name]
    score,LL,y_pred = puntaje(model,Xs,y)
    _, ax = plt.subplots(figsize=(4, 3))
    # Plot also the training points
    plt.scatter(
        X[:, 0], X[:, 1],
        c=y_pred,
        edgecolors="k",
        cmap=plt.cm.Paired)
    ax.set_xlabel("X1")
    ax.set_ylabel("X2")
    ax.set_title(f"Regresión Logistica {name}")
    plt.savefig(here / f"img/montecarlo-ponderado-{name}.svg")
    plt.show()


@click.command()
@click.option(
    "--data",
    type=click.Path(exists=True),
    help="ruta de datapath")
@click.option(
    "--model",
    type=click.Path(exists=True), 
    help="modelpath")
def run(data, model):
    here = Path(__file__).parent 
    datapath = Path(data)
    modelpath = Path(model)
    if datapath.exists():
        (X, Xp1, Xp3), y = read_data(datapath)
        #read models and select
        Xp = {
            "lineal": Xp1,
            "poly3": Xp3          
        }
        print(X.shape)
    else:
        print("Path to dataset {datapath} does not exists")
    #run_parallel(datapath, iterations, stop_condition)
    if modelpath.exists():
        the_model = {}
        models = load_dataset(modelpath)
        print(models.keys())
        for poly_type, group in models.items():
            print(poly_type)
            for pond, models in group.items():
                predelta = -2
                for delta, model in models.items():
                    score, LL, y_pred = puntaje(model, Xp[poly_type], y)
                    print(poly_type, pond, (.5+delta), score, LL)
                    if predelta <=0 and delta>=0:
                        the_model[poly_type] = model
                    predelta = delta
        print("The models")
        print(the_model)
        for poly_type, model in the_model.items():
            plot_data(poly_type, model, X, Xp, y)

        # print("Akaike P2=>",model_p1.aic, "P3=>",model_p3.aic)
        # print("BIC P2=>",model_p1.bic, "P3=>",model_p3.bic)

        # print("Akaike P2=>",model_p1.aic, "P3=>",model_p3.aic)
        # print("BIC P2=>",model_p1.bic, "P3=>",model_p3.bic)
    else:
        print("Path to model {modelpath} does nott exists")

if __name__ == "__main__":
    run()

from typing import List, Dict, Union
from mpmath import mp as math
# mpmath permite calculos de coma flotante eficiente
from dataclasses import dataclass, field, asdict
from typing import List
import numpy.typing as npt
import numpy as np
from rich import print
from sklearn.metrics import accuracy_score
from pathlib import Path
import pickle 
import random
from copy import deepcopy
from timeit import default_timer as timer

Matrix  = npt.ArrayLike
MatrixNxD = Matrix # like X dataset 
Matrix1xD = Matrix # like X row 
Matrix1xD_plus_one = Matrix # like X row and 1
MatrixNx1_C = List[str] # a list of class names (tags)
MatrixNx1_OH = Matrix # matrix fot list of output parsed to onehot row
MatrixNxC = Matrix # matrix 
MatrixNx1 = Matrix
Matrix1xC = Matrix
MatrixDxC = Matrix
MatrixD_plus_onexC = Matrix
 


def softmax(vals:Matrix1xC)-> Matrix1xC:
    """
    Compress all values of x to [0,1]
    """
    exps = [math.exp(val) for val in vals]
    total = sum(exps)
    return np.array([e/total for e in exps])


def softmaxW(x:Matrix1xD_plus_one, W: MatrixD_plus_onexC)-> Matrix1xC:
    """
    Compress all values of x to [0,1]
    # [x1, x2,..., xD, 1]
    # xi = x.T
    # vals = []
    # # for every column class:
    # for wc in W.T:
    #     val = xi @ wc
    #     vals.append(val)
    # wc = [w1,w2,...,WD, b0]
    """
    return softmax([x.T @ wc for wc in W.T])


def label_to_onehot(y:MatrixNx1_C) -> Dict[str, int]:
    """
    Give an unique number for every label
    the number is power of 2: 2**i
    """
    uniques = sorted(set(y))
    start = 1
    result = {}
    n = len(uniques)
    for i,value in enumerate(uniques):
        result[value] = tuple(
            [0 for _ in range(n-i-1)] + 
            [int(v) for v in bin(start).replace("0b", "")])
        start = start << 1
    return result


def random_vector(X):
    numbers = [i for i,_ in enumerate(X)]
    random.shuffle(numbers)
    return numbers

@dataclass
class LogLikehood:
    name: str
    iteration: int
    value: float
    eta: float
    error_ll: float
    error_slope: float
    duration: float
    features: int
    size_data: int
    
    def dict(self):
        return asdict(self)
    
    @property
    def aic(self) -> float:
        """
        Criterio de información de Akaike

        criterio = 2 * log(p(D|thetha)) - 2 ord(thetha)

        Entrega un índice entre la bondad de ajuste y la complejidad del
        modelo.

        Se basa en la entropía de información de Akaike

        Thetha: vector del estimador R^d (d caracteristicas)

        El AIC se define por:

        M : modelo estadístic d-parametros
        D: conjunto de los datos xi 
        LL_D: verosimilitud EMV asociada a D
        AIC(M,D) := 2*d -2 log(LL_D
        """
        AIC = 2*self.features - 2 * self.value
        return AIC

    @property
    def bic(self):
        """
        Es un criterio basado en estudio bayesiano del modelo.
        N : Muestras de X 
        d: features
        BIC :=  d * log(N) - 2 * log(LL_D)

        """
        BIC = self.features*math.log(self.size_data) - 2 * self.value

        return BIC



@dataclass
class LogisticRegression:
    eta: float = 0.1
    iterations:int = 1000
    W:Matrix = field(default_factory = lambda: np.array([]))
    do_history:bool = False
    LL_list:List[LogLikehood] = field(default_factory = list)
    data_name:str = "test"
    refresh:bool = False
    stop_condition: float = 5e-4 
    clean:bool =  True
    ok_clases: bool = False
    # must has  phi(x) = 1, característica constante

    #def __post__init__(self):

    @property
    def save_path(self):
        path = Path(__file__).parent / "model" / f"logistic_regression_{self.data_name}_{self.eta:10.6E}_{self.iterations}.pydat"
        return path
        
    def get_y_values(self, y:MatrixNx1_C) -> MatrixNx1_OH:
        """
        Given a vector, returns the one hoy value for the vector
        """
        return np.array([self.y_clases[valor] for valor in y])


    def set_clases(self, y_clases):
        self.create_y_classes(y_clases)
        self.ok_clases = True

    def create_y_classes(self, y:MatrixNx1_C):
        self.y_clases = label_to_onehot(y)
        self.bit_to_class = {bits: class_name 
                             for class_name, bits in self.y_clases.items()}

    def get_class(self, bits:Matrix1xC) -> str:
        return self.bit_to_class.get(tuple(bits))

    def start_w(self, X:MatrixNxD, y:Union[MatrixNx1_C, MatrixNx1_OH]) -> MatrixD_plus_onexC:
        n,d = X.shape
        c = len(self.y_clases)
        # the siz of W must be d features and the constant +1
        shape = (d+1, c)
        self.W = np.zeros(shape)
        return self.W

    def predict_x_row(self, x:Matrix1xD_plus_one) -> Matrix1xC:
        sig_x = softmaxW(x, self.W)
        return sig_x

    def predict_probas(self, X:MatrixNxD) -> MatrixNxC:
        # input : matriz de datos X que contiene vectores x_1 , x_2 , ... , x_n
        # return : matriz de probabilidades de clase para los datos X
        # por cada nuevo ajuste de self.W el resultado cambia para el
        # mismo X
        # insert the 1 to x values
        return np.array([self.predict_x_row(np.append(x,1)) for x in X])


    def predict (self , X:Matrix) -> MatrixNxC: 
        # input : matriz de datos X que contiene vectores x_1 , x_2 , ... , x_n
        # return : vector de clases predichas para los datos X
        y_predict = self.predict_probas(X)
        p = y_predict.argmax(axis=1)
        one_hot_pred = [[0 if j!=p[i] else 1 for j,e in
                         enumerate(row)] for i,row in
                        enumerate(y_predict)]
        y_pred = np.array([self.get_class(row) for row in one_hot_pred])
        return y_pred

    def log_likelihood ( self , X:MatrixNxD , y:MatrixNx1_C) -> float: 
        # input : datos ( instancias y etiquetas )
        # return : log verosimilitud del modelo considerando el valor
        # actual de W
        y_bits = self.get_y_values(y)
        # FN para calcular la operación y[i] * wct @ X[i]
        # wct @ X[i] :: float
        # y[i] :: y logic one hot
        def ywx(i:int, x:Matrix1xD_plus_one) -> float:
            return  sum(
                [
                    val * ( self.W.T[c] @ x) for c, val in enumerate(y_bits[i])
                ]
            )


        # parte II: log ( suma ( exponente ( wcT x)))
        def log_sum_exp_wx(x:Matrix1xD_plus_one) -> float: 
            return math.log(
                sum(
                    [math.exp(wx) for wx in softmaxW(x, self.W)]
                )
            )
        # log likehood 
        # recorre cada fila de X, obteniendo 'i' y 'X[i]'
        # opera ywx y log_sum_exp para i, X[i]
        LL =  sum(
            [ywx(i,np.append(x, 1)) - log_sum_exp_wx(np.append(x, 1)) 
             for i, x in enumerate(X)]
        )
        return LL        
                     

    def derive_log_likehood(self, X:MatrixNxD, y:MatrixNx1_OH, j:int) -> MatrixD_plus_onexC:
        # y: (I(yi=1), ....,I(yi=C-1))), largo C-1
        # mu = [p(yi=1|xi,W),...,p(yi=1|xi,W)]
        # prediccion, matrices [mu_1, mu_2, ..., mu_C-1].T
        # resultado cambia porque self.W termina cambiando
        newX = np.append(X[j], 1)
        mu = self.predict_x_row(newX)
        # diferencia.
        delta = y[j] - mu
        # newX is D+1 length, delta is 1xC: -> D+1 x C
        q  = np.dot(mu, delta)
        return np.array([xv*(delta) for xv in newX])


    def just_fit(self, X:MatrixNxD, y:MatrixNx1_C) -> 'LogisticRegression':
        y_bits = self.get_y_values(y)
        self.y_logic = y_bits
        errors = [1]
        error_slope = []
        buffer = 10
        error_slope_val = 1
        n_size, features = X.shape 
        for i in range(self.iterations):
            random_index = random_vector(X)
            preW = deepcopy(self.W)
            start = timer()
            for j in random_index:
                # eta_i: W.T * xi vector de Cx1 valores 
                self.W = self.W + self.eta * self.derive_log_likehood(
                    X, y_bits, j)                
                # save LL?
            end = timer()
            prev_ll = 1
            if self.LL_list:
                prev_ll = self.LL_list[-1].value
            ll_value = self.log_likelihood(
                        X,
                        y)
            error_prev = 1
            if errors:
                error_prev = errors[-1]
            error_ll = abs(ll_value - prev_ll)
            errors.append(error_ll)
            buffsize = min(len(errors), buffer)
            if len(errors)>buffer:
                errors.pop(0)
            if len(error_slope)>buffer:
                error_slope.pop(0)
            error_slope.append(abs(error_ll-error_prev))
            
            if error_slope:
                
                error_slope_val = np.array(error_slope[-buffsize:]).mean()
                print("Iteration", i, "error splope", error_slope_val)

            duration = end - start
            print("Iteration", i, "error splope", error_slope_val, "duration", duration)

            if self.do_history:
                ll = LogLikehood(
                    self.__class__.__name__,
                    i,
                    ll_value,
                    self.eta,
                    error_ll,
                    error_slope_val,
                    duration,
                    features,
                    n_size
                )
                self.LL_list.append(ll)
            prev_ll = ll_value

            if error_slope_val <= 500*self.eta:
                new = self.eta*.9
                self.set_eta(new)
            if error_slope_val < self.stop_condition:
                print(f"I {i}, error {error_ll}, mean error slope {error_slope_val}, eta {self.eta}, duracion {duration}")
                break

    def fit(self, X:MatrixNxD, y:MatrixNx1_C, clean=False) -> 'LogisticRegression':
        # input : datos ( instancias y etiquetas )
        # objetivo : encontrar W mediante SGD: descenso por gradiente
        # estocástico
        if self.save_path.exists() and not self.refresh:
            return self.load()
        else:
            if clean:
                if not self.ok_clases:
                    self.create_y_classes(y)
                self.W = self.start_w(X, y)
            self.just_fit(X,y)
        return self
    
    @property
    def get_y(self):
        return self.y_logic

    def set_iterations(self, n:int):
        self.iterarions = n

    def set_eta(self, eta:float):
        self.eta = eta
    

    def clases(self, y_predict):
        clases = [tuple(1 if float(v) > 0 else 0 for v in row) 
                  for row in y_predict] 
        names = [self.get_class(c) for c in clases]
        return names


    @property
    def one_hot(self):
        return self.y_clases

    @property
    def params(self):
        return self.W

    @property
    def history(self):
        return self.LL_list

    @property
    def aic(self):
        return self.LL_list[-1].aic

    @property
    def bic(self):
        return self.LL_list[-1].bic


    def set_params(self, W, y_clases, ok_clases, bit_to_class):
        self.W = W
        self.y_clases = y_clases
        self.ok_clases = ok_clases
        self.bit_to_class = bit_to_class
        print("Now do :: model.fit(X,y,clean=False)")
        
    def score(self, y_test:MatrixNx1_C, y_pred:MatrixNx1_C) -> float:
        return accuracy_score(y_test, y_pred)

    def dict(self):
        return asdict(self)

    def load(self) -> List[LogLikehood]:
        data_bytes = self.save_path.read_bytes()
        return pickle.loads(data_bytes)

    def save(self):
        self.save_path.parent.mkdir(exist_ok=True, parents=True)
        dump = pickle.dumps(self)
        self.save_path.write_bytes(dump)
        return self.save_path

from logistic_regression import softmax, softmaxW,label_to_onehot, LogisticRegression
import random
import unittest
from networktools.library import my_random_string
from rich import print
import numpy as np

class TestLinearRegression(unittest.TestCase):
    def test_softmax(self):
        N = random.randint(1,10000)
        x = [random.uniform(-10000,10000) for _ in range(N)]
        result = softmax(x)
        assert all([0<= e <=1 for e in result]), "Softmax function is incorrect"

    def test_label_to_onehot(self):
        names = ["perro", "gato", "gato", "pavo", "perro"]
        onehot = {"gato":(0,0,1),
                  "pavo":(0,1,0),
                  "perro":(1,0,0)}
        result = label_to_onehot(names)
        lista = sorted(result.keys())
        valcomp = [onehot[k]==result[k] for k in lista]
        assert all(valcomp), "The label to onehot has incorrect assignment"
        
    def test_one_hot_linearreg(self):
        logreg = LogisticRegression()
        y = ["perro", "gato", "gato", "pavo", "perro"]
        #X = [[1,1,2],[0,3,0],[0,6,0],[-1,-1,0],[2,2,4]]
        logreg.create_y_classes(y)
        clases = label_to_onehot(y)
        assert clases==logreg.y_clases, "No genera misma tabla one-hot"

    def test_get_y_values(self):
        logreg = LogisticRegression()
        y = ["perro", "gato", "gato", "pavo", "perro"]
        logreg.create_y_classes(y)
        onehot = {"gato":[0,0,1],
                  "pavo":[0,1,0],
                  "perro":[1,0,0]}

        matrix = np.array([onehot[k] for k in y])
        #X = [[1,1,2],[0,3,0],[0,6,0],[-1,-1,0],[2,2,4]]
        result = logreg.get_y_values(y)
        assert (matrix==result).all(), "Not generate correct onehot vector"


    def test_start_w(self):
        logreg = LogisticRegression()
        y = ["perro", "gato", "gato", "pavo", "perro"]
        logreg.create_y_classes(y)
        result = logreg.get_y_values(y)
        X = np.array([[1,1,2],[0,3,0],[0,6,0],[-1,-1,0],[2,2,4]])
        logregW = logreg.start_w(X,result)
        W = np.zeros((4, 3))
        assert (W.shape==logregW.shape and
                np.count_nonzero(W)==np.count_nonzero(logregW)), "No es el mismo W inicial"


    def test_predict_probas(self):
        logreg = LogisticRegression()
        y = ["perro", "gato", "gato", "pavo", "perro"]
        logreg.create_y_classes(y)
        y_bites = logreg.get_y_values(y)
        X = np.array([[1,1,2],[0,3,0],[0,6,0],[-1,-1,0],[2,2,4]])
        logregW = logreg.start_w(X, y_bites)
        prediction = np.array([softmaxW(np.append(x,1), logregW) for x in X])
        sigX = logreg.predict_probas(X)
        assert  np.array_equal(sigX, prediction),  "No es misma matrix predicción"

    def test_derive_ll(self):
        logreg = LogisticRegression()
        y = ["perro", "gato", "gato", "pavo", "perro"]
        X = np.array([[1,1,2],[0,3,0],[0,6,0],[-1,-1,0],[2,2,4]])
        logreg.create_y_classes(y)
        result = logreg.get_y_values(y)
        logregW = logreg.start_w(X, result)
        mu = logreg.predict_probas(X)
        n, c = X.shape
        # el producto kronecker se toma como ya teasteado por numpy
        onehot = {"gato":[0,0,1],
                  "pavo":[0,1,0],
                  "perro":[1,0,0]}
        # y value real as bites
        matrix = np.array([onehot[k] for k in y])
        delta =  matrix - mu
        newX = np.array([np.append(x,1) for x in X])
        dll = newX.T @ delta
        dll_logreg = logreg.derive_log_likehood(X, matrix)
        equals = np.array_equal(dll, dll_logreg)
        assert  equals,  "No es misma matrix derivada log verosimilitud"


    def test_fit(self):
        # test if returns an array and get class names
        logreg = LogisticRegression()
        y = ["perro", "gato", "gato", "pavo", "perro"]
        X = np.array([[1,1,2],[0,3,0],[0,6,0],[-1,-1,0],[2,2,4]])
        W = logreg.fit(X,y)
        y_predict = logreg.predict(X)
        clases = [tuple(1 if float(v) > 0 else 0 for v in row) for row in y_predict] 
        names = [logreg.get_class(c) for c in clases]
        assert any([n in y for n in names]), "Fit doesn't work"


    def test_loglikehood(self):
        pass

if __name__ == "__main__":
    unittest.main()

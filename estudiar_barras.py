from pathlib import Path
from dataclasses import dataclass
from rich import print
import pickle
from working_data import DatoElectrico, TipoBarra
from itertools import groupby
from statistics import fmean, stdev
import numpy as np

sep = "__"
dataset_path = Path(__file__).parent / "dataset"


from typing import List


@dataclass
class DatasetStation:
    station: str
    dataset: List[Path]

    def append(self,filename:Path):
        self.dataset.append(filename)

    def get(self, index:int):
        try:
            return self.dataset[index]
        except IndexError as ie:
            raise ie 

    def read(self, index:int):
        try:
            datafile = self.get(index)
            return pickle.loads(datafile.read_bytes())
        except IndexError as ie:
            raise ie 
        

    def __iter__(self):
        return iter(self.dataset)

    

def read_directory(default=Path(__file__).parent / "directory.pydat"):
    directory_path = default
    directory = {}
    if not directory_path.exists():
        for file_name in dataset_path.rglob("*.pydat"):
            station = file_name.parent.name.split(sep)[0]
            if station not in directory:
                directory[station] = DatasetStation(station, [])
            dataset = directory[station]
            dataset.append(file_name)

        with directory_path.open("wb") as f:
            pickle.dump(directory, f)

    else:
        directory = pickle.loads(directory_path.read_bytes())
    return directory


def analisis_temporal(dataset, nombre):
    keys = {"bloque_horario", "temporada", "diario", "date"}
    stats = []
    filtro_tipo = lambda item: item.tipo
    dataset_by_tipo  = sorted(dataset, key=filtro_tipo)
    for tipo, grupo_t in groupby(dataset_by_tipo, key=filtro_tipo): 
        grupo_tipo = list(grupo_t)
        for analisis in keys:
            filtro = lambda item: item.temporal[analisis]
            dataset_ord = sorted(grupo_tipo, key=filtro)
            for key, group in groupby(dataset_ord, key=filtro):
                data = np.array([item.medida_horaria for item in group])
                media = np.mean(data)
                std = np.std(data)
                median = np.median(data)
                data = {"nombre":nombre,
                        "tipo":tipo,
                        "analisis":analisis,
                        "llave":key,
                        "media": media,
                        "dev_std": std,
                        "median":median}
                stats.append(data)
    return stats

from datetime import datetime

def save_stats(stats, name):
    now = datetime.now().isoformat()
    stats_dir = Path(__file__).parent / "stats" / now
    stats_dir.mkdir(exist_ok=True)
    stats_path = stats_dir / f"{name}.pydat"
    databytes = pickle.dumps(stats)
    stats_path.write_bytes(databytes)

def obtain_data(lista, directory):
    dataset = {}
    for name in lista:
        stats = {}
        dataset_station = directory[name]        
        total_data = []
        dataset[name] =  total_data
        for index, item in enumerate(dataset_station): 
            data = dataset_station.read(index)
            total_data += data
        stats[name] = analisis_temporal(total_data, name)
        # AQUI CORRER ALGUNA OPERACION SOBRE LOS DATOS: total_data
        # podria ser un filtro por fecha, operar sobre eso, etc
        
    # saving stats
    
    print(dataset)
    return dataset


    
def obtain_data_all(lista, directory):
    for name in lista:
        stats = {}
        dataset_station = directory[name]
        total_data = []
        for index, item in enumerate(dataset_station):
            data = dataset_station.read(index)
            total_data += data
        stats[name] = analisis_temporal(total_data, name)
        # AQUI CORRER ALGUNA OPERACION SOBRE LOS DATOS: total_data
        # podria ser un filtro por fecha, operar sobre eso, etc
        print(f"Cantidad de datos en {name}",len(total_data))
        save_stats(stats, name)
    # saving stats


def read_data(todas:bool, name:str=None, listar:bool=False):
    directory = read_directory()
    lista = []
    if todas:
        lista = list(directory.keys())
    if listar:
        print(list(directory.keys()))
    STATION = ""
    if name:
        STATION = name
    elif name and not todas:
        STATION = input("Escribe la estacion a estudiar ")

    if STATION in directory and not todas:
        lista.append(STATION)
    else:
        print(f"No existe esa estacion {STATION}")
    if todas:
        print("Hacer todas las barras")
        return obtain_data_all(lista, directory)
    print("Hacer seleccion", lista)
    return obtain_data(lista, directory)
    

import click 

@click.command()
@click.option('--todas/--no-todas', default=False, help="Todas las estaciones")
@click.option('--name', default=None, help="Name of station")
@click.option('--listar/--no-listar', default=False, help="Listar estaciones")
def run(todas, name, listar):
    read_data(todas, name.upper() if name else "", listar)

if __name__ == "__main__":
    run()

# standar lib
from pathlib import Path
from collections import namedtuple
from dataclasses import dataclass
import csv
import math
# contrib
import requests
from bs4 import BeautifulSoup
from rich import print

path_web = Path("geochile.html")

if not path_web.exists():
    url = "https://es.wikipedia.org/wiki/Anexo:Comunas_de_Chile"
    r = requests.get(url)
    content = r.content
    with path_web.open("wb") as f:
        f.write(content)
else:
    content = path_web.read_bytes()

    
soup = BeautifulSoup(content, 'html.parser')

table = soup.find("table")


@dataclass
class PositionLatLonDeg:
    degree: int
    minutes: int
    seconds: float

    @property
    def decimals(self):
        a =  self.seconds / 60
        b = (self.minutes + a) / 60
        c = self.degree + math.copysign(1,self.degree)*b
        return c

    def __repr__(self):
        return f"{self.decimals}"

    
def position2decimal(position):
    pos_deg = position.find("°")
    deg = int(position[:pos_deg])
    pos_min = position.find("'")
    minut = int(position[pos_deg+1:pos_min])
    pos_sec = position.find('\"')
    sec = float(position[pos_min+1:pos_sec])
    return PositionLatLonDeg(deg, minut, sec)


FieldOp = namedtuple("FieldOp", "name operation")

positions = {
    0: FieldOp("cut", lambda e: e.find("code").get_text() ),
    1: FieldOp("nombre", lambda e: e.find("a").get_text()),
    3: FieldOp("provincia", lambda e: e.find("a").get_text()),
    4: FieldOp("region", lambda e: e.find_all("a")[-1].get_text()),
    5: FieldOp("superficie",lambda e:float(e.get_text()
                                           .strip()
                                           .replace(".","")
                                           .replace(",","."))),
    6: FieldOp("poblacion",lambda e:float(e.get_text()
                                           .strip()
                                           .replace(".","")
                                           .replace(",","."))),
    7: FieldOp("densidad",lambda e:float(e.get_text()
                                           .strip()
                                           .replace(".","")
                                           .replace(",","."))),
    10: FieldOp("latitud",lambda e: position2decimal(e.get_text().strip())),
    11: FieldOp("longitud",lambda e: position2decimal(e.get_text().strip()))
}

fields = [f.name for f in positions.values()]

data_csv = Path("chile_geo.csv")

with data_csv.open("w") as f:
    writer = csv.DictWriter(f,fieldnames=fields,delimiter=";")
    writer.writeheader()
    for row in table.tbody.find_all("tr"):
        columns = row.find_all("td")
        if columns:
            data = {fo.name:fo.operation(columns[k]) for k, fo in positions.items() }
            print(data)
            writer.writerow(data)

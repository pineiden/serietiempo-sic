
import pickle
from pathlib import Path
from conexion_electrica import Table, Barra, ColumnType, run
from rich import print
import random

from dataclasses import dataclass
from datetime import datetime, timedelta
import pytz

from enum import Enum

class TipoBarra(Enum):
    G = 1
    L = 2
    L_D = 3  
    L_S = 4
    R = 4
    T = 6
    N = 6

    @property
    def description(self):
        text = {
            1: "Proviene de generación",
            2: "Libre",
            3: "Libre de distribuidora",
            4: "Libre de suministrador",
            5: "Consumo regulado",
            6: "Circula por transmision",
            7: "No recuerdo"
        }
        return text.get(self.value)


    def __str__(self):
        return f"{self.name} :: {self.description}"
    
@dataclass
class DatoElectrico:
    barra: Barra
    tipo: TipoBarra
    periodo:str
    hora_mensual: int
    medida_horaria: float
    cmg: float

    def __post_init__(self):
        self.tipo = TipoBarra[self.tipo]

    @property
    def dt_gen(self):
        year = int(str(self.periodo)[0:4])
        month = int(str(self.periodo)[4:6])
        day_0 = datetime(year,month,1)
        datetime_gen =  day_0 + timedelta(hours=self.hora_mensual)
        return datetime_gen
    
    def __repr__(self):
        return f"{self.barra}|{self.tipo} -> {self.dt_gen}-> {self.medida_horaria} -> {self.cmg}"

    def __add__(self, dato):
        self.medida_horaria += dato.medida_horaria
        return self

    def to_dict(self):
        diccionario = {
            "barra": self.barra.name,
            "tipo": self.tipo.value,
            "periodo": self.periodo,
            "hora_mensual": self.hora_mensual,
            "dt_gen": self.dt_gen,
            "cmf": self.cmg,
            "medida_horaria": self.medida_horaria
        }
        return diccionario
    
def min_max_periodo(conn):
        query = f"""
select distinct(periodo) from valorizado
        """
        periodos = []
        rs = conn.execute(query)
        for row in rs:
            data = int(row[0])
            periodos.append(data)
        return periodos
    
def obtener_timeserie(conn, tabla, barra,  start=0, end=800, delta=80):
    datapath = Path(f"dataset_{tabla.name}_{barra.name}") 
    print("Obteniendo timeseria de", barra, "en ", tabla)
    datapath.mkdir(exist_ok=True, parents=True)
    for periodo in min_max_periodo(conn):
        print(datapath, periodo)
        dataset = []
        for value in range(start, end, delta):
           query = f"""
select tipo1 as tipo, periodo, hora_mensual, medidahoraria2 as medida_horaria, cmg_peso_kwh as cmg
from {tabla.name} where  nombre_barra='{barra.name}'
and periodo = '{periodo}' and hora_mensual between {value} and {value+delta}
order by periodo, hora_mensual;
"""
           # print(query)
           rs = conn.execute(query)
           for row in rs:
               data = (barra, *row)
               dataset.append(DatoElectrico(*data))

        print("Cantidad elementos dataset", dataset)
        dataset_periodic = datapath / f"{periodo}.pydat"
        with dataset_periodic.open("wb") as f:
            pickle.dump(dataset, f)
    return dataset              # 


if __name__== '__main__':
    valorizacion = Path("valorizacion.dat")
    if valorizacion.stat().st_size > 0:  
        data_bytes = valorizacion.read_bytes()
        data = pickle.loads(data_bytes)
        tabla = data["tablas"][0]
        barras = data["barras"][tabla]
        barra = random.choice(barras)
        engine = run()
        with engine.connect() as conn:
            dataset = obtener_timeserie(conn, tabla, barra)
    else:
        print("Archivo vacio", valorizacion)

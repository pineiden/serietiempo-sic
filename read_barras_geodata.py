import re
from pydantic.dataclasses import dataclass
import shapely
from conexion_electrica import create_conn
from pathlib import Path
import json
from rich import print
import pickle
from shapely.geometry import Point
from shapely.ops import transform
import pyproj
from functools import partial
from pyproj import Proj
from dataclasses import field, asdict
from shapely.geometry import Point
from shapely.geometry import shape, Polygon, MultiPolygon
from typing import List, Dict, Any, Optional, Set, Union


# form lat lon
wgs84 = pyproj.CRS('EPSG:4326')
utm = pyproj.CRS('EPSG:32618')
sirgas_chile = pyproj.CRS("epsg:5360")
utm = pyproj.CRS('EPSG:32618')


project = pyproj.Transformer.from_crs(wgs84, utm, always_xy=True).transform
project_chile = pyproj.Transformer.from_crs(sirgas_chile, utm, always_xy=True).transform

@dataclass
class GeoInfo:
    region: str
    cut_region: int
    provincia: str
    cut_provincia: int
    comuna: str
    cut_comuna: int
    superficie: float
    comuna_geo: Optional[Any] = field(default_factory=lambda: Polygon([0,0]))
    poblacion: Optional[float] = None
    densidad: Optional[float] = None

    
    def __iter__(self):
        return iter(self.barras)

    def set_comuna_geo(self,multi_polygon:MultiPolygon):
        self.comuna_geo = multi_polygon

    def set_poblacion(self, poblacion:float):
        self.poblacion = poblacion

    def set_densidad(self, densidad:float):
        self.densidad = densidad
        
    @property
    def center(self) -> Point:
        if type(self.comuna_geo) in {Polygon, MultiPolygon} :
            return self.comuna_geo.centroid

    def dict(self):
        data =  asdict(self)
        data["center"] = self.center
        return data

        
@dataclass
class VoronoiGroup:
    code: str
    geo: Any = field(default_factory=list)

@dataclass
class InfoBarra:
    id: int
    subestacion: str
    codigo: str
    nemotecnico: str
    id_subestacion: int
    latitud: float
    longitud: float
    geodata: Optional[GeoInfo] = None
    voronoi_group: Optional[VoronoiGroup] = None
    name_barra: str= ""
    
    @property
    def point(self):
        return transform(project_chile, Point(self.longitud,
                                              self.latitud))

    def set_geodata(self, data:GeoInfo):
        self.geodata = data

    def set_voronoi(self, data:VoronoiGroup):
        self.voronoi_group = data

    def set_name_barra(self, name:str):
        self.name_barra = name
        
def obtain_info_barras(conn):
    barras_path = Path(__file__).parent / "info_barras.pydat"
    barras = {}
    if not barras_path.exists():
        query_text = """select id,subestacion_nombre, codigo, nemotecnico, sg.id_subestacion, sg.latitud, sg.longitud from barras
    inner join subestaciones_geolocalizacion sg on barras.id_subestacion=sg.id_subestacion;
    """
        rs = conn.execute(query_text)
        for row in rs:
            barra = InfoBarra(*row)
            barras[barra.subestacion] = barra
        data_bytes = pickle.dumps(barras)
        barras_path.write_bytes(data_bytes)
    else:
        barras = pickle.loads(barras_path.read_bytes())
    return barras
   


if __name__ == "__main__":
    datafile = Path(__file__).parent / "datafile.json"
    engine, conn = create_conn(json.loads(datafile.read_text()))
    barras = obtain_info_barras(conn)
    print(barras)
    for name, barra in barras.items():
        print(name, barra.point)

import asyncio
import pickle
from pathlib import Path
from conexion_electrica import Table, BarraTabla, ColumnType, run_engine
from rich import print
import random
from dataclasses import dataclass as std_dataclass, asdict
from dataclasses import dataclass
from datetime import datetime, timedelta
import pytz
import multiprocessing
from feriados import read_feriados

from enum import Enum, IntEnum
from functools import total_ordering

NCPU = multiprocessing.cpu_count()
WORKERS = min(NCPU - 2, 4)

@total_ordering
class TipoBarra(IntEnum):
    G = 1
    L = 2
    L_D = 3  
    L_S = 4
    R = 4
    T = 6
    N = 6

    @property
    def description(self):
        text = {
            1: "Proviene de generación",
            2: "Libre",
            3: "Libre de distribuidora",
            4: "Libre de suministrador",
            5: "Consumo regulado",
            6: "Circula por transmision",
            7: "No recuerdo"
        }
        return text.get(self.value)

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

    def __str__(self):
        return f"{self.name} :: {self.description}"

FERIADOS, DOMINGOS, SABADOS = read_feriados()
DATES_FERIADOS = {f.fecha for f in FERIADOS} | DOMINGOS
DESCANZO = SABADOS

@dataclass
class DatoElectrico:
    barra: BarraTabla
    tipo: TipoBarra
    periodo: str
    hora_mensual: int
    medida_horaria: float
    cmg: float

    def __post_init__(self):
        self.tipo = TipoBarra[self.tipo]

    @property
    def dt_gen(self):
        year = int(str(self.periodo)[0:4])
        month = int(str(self.periodo)[4:6])
        day_0 = datetime(year, month, 1)
        datetime_gen =  day_0 + timedelta(hours=self.hora_mensual)
        return datetime_gen

    def time_tags(self):
        dt = self.dt_gen
        year = dt.year
        month = dt.month
        day = dt.day
        hour = dt.hour
        feriado = dt in DATES_FERIADOS
        descanzo = dt in DESCANZO
        laboral = not (descanzo or feriado)
        temporada = "verano"
        if month in {4, 5, 6}:
            temporada_= "otoño"
        elif month in {7, 8, 9}:
            temporada = "invierno"
        elif month in {10, 11}:
            temporada = "primavera"
        bloque_horario = "noche"
        if hour in range(8, 14):
            bloque_horario = "mañana"
        elif hour in range(14, 20):
            bloque_horario = "tarde"
        elif hour in range(20, 23):
            bloque_horario = "tarde-noche"
        diario = {
                "laboral": laboral,
                "descanzo": descanzo,
                "feriado": feriado
            }
        return {
            "dt_gen": dt,
            "date": self.dt_gen.date(),            
            "bloque_horario": bloque_horario,
            "temporada": temporada,
            "diario": [key for key, v in diario.items() if v].pop(),
            **diario
        }
    @property
    def temporal(self):
        return self.time_tags()
    
    def dict(self):
        data =  asdict(self)
        data.update(self.temporal)
        return data

    def __add__(self, dato):
        if self.hora_mensual == dato.hora_mensual:
            self.medida_horaria += dato.medida_horaria
        return self

    def to_dict(self):
        diccionario = {
            "barra": self.barra.name,
            "tipo": self.tipo.value,
            "periodo": self.periodo,
            "hora_mensual": self.hora_mensual,
            "date": self.dt_gen.date(),            
            "dt_gen": self.dt_gen,
            "cmf": self.cmg,
            "medida_horaria": self.medida_horaria,
            "time_tags": self.temporal
        }
        return diccionario

def get_data(data, tabla, grupo_barras):
    engine, conn = run_engine(data)
    for barra in grupo_barras:
        print("Obtaining barra", barra)
        dataset = obtener_timeserie(conn, tabla, barra)
    conn.close()

    
def distribuir(dataset, workers):
    groups = {i: [] for i in range(workers)}
    counter = 0
    while dataset:
        data = dataset.pop()
        groups[counter].append(data)
        counter += 1
        if counter == workers:
            counter = 0
    return groups
    
def min_max_periodo(conn):
        query = f"""
select distinct(periodo) from valorizado
        """
        periodos = []
        rs = conn.execute(query)
        for row in rs:
            data = int(row[0])
            periodos.append(data)
        return periodos
  
def obtener_timeserie(conn, tabla, barra,  start=0, end=800, delta=80):
    datapath = Path(f"dataset/{tabla.name}/{barra.name}")
    print("Datapath", datapath)
    print("Obteniendo timeseria de", barra, "en ", tabla)
    datapath.mkdir(exist_ok=True, parents=True)
    for periodo in min_max_periodo(conn):
        print(datapath, periodo)
        dataset_periodic = datapath / f"{periodo}.pydat"
        # not query to db if exists
        if not dataset_periodic.exists():
            dataset = []
            for value in range(start, end, delta):
               query = f"""
    select tipo1 as tipo, periodo, hora_mensual, medidahoraria2 as medida_horaria, cmg_peso_kwh as cmg
    from {tabla.name} where  nombre_barra='{barra.name}'
    and periodo = '{periodo}' and hora_mensual between {value} and {value+delta}
    order by periodo, hora_mensual;
    """
               # print(query)

               rs = conn.execute(query)
               for row in rs:
                   data = (barra, *row)
                   dataset.append(DatoElectrico(*data))

            print("Cantidad elementos dataset", dataset)
            with dataset_periodic.open("wb") as f:
                pickle.dump(dataset, f)
    return dataset              # 


import click
import json 
from functools import partial
import concurrent.futures

@click.command()
@click.option("-b/-nb","--barra-random/--no-barra-random", default=True)
@click.option("--datafile",
              default=Path("./datafile.json"),
              type=click.Path())
def run(barra_random, datafile):
    valorizacion = Path("valorizacion.dat")
    if valorizacion.stat().st_size > 0:  
        data_bytes = valorizacion.read_bytes()
        data = pickle.loads(data_bytes)
        tabla = data["tablas"][0]
        barras = data["barras"][tabla]
        selection = []
        # aleatorio elige una
        if barra_random:
            barra = random.choice(barras)
            selection.append(barra)
        else:
            barra = None
            for i, sbarra in enumerate(barras):
                print(i, sbarra)
            msg = "Seleccion barra por índice  (ALL/o index)"
            OK = {"ALL"} | {str(i) for i,v in enumerate(barras)}
            while (index:=input(msg)) in OK:
                if index.isdigit():
                    if 0 <= int(index) <= i:
                        print("Adding", index)
                        barra = barras[int(index)]
                        selection.append(barra)
                if index == "ALL":
                    selection += barras
                    break
        data = json.loads(datafile.read_text())
        lista = [s for s in selection]
        grupos = distribuir(selection, WORKERS)
        print(grupos)
        loop = asyncio.get_event_loop()
        if lista:
            print("Running in executor")
            with concurrent.futures.ProcessPoolExecutor() as pool:
                for worker, grupo in grupos.items():
                    print("Definiendo executor", get_data, grupo)
                    loop.run_in_executor(
                        pool,
                        partial(get_data, data, tabla, grupo))

        loop.run_forever()
    else:
        print("Archivo vacio", valorizacion)

if __name__== '__main__':
    run()

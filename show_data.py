
import pickle
from pathlib import Path
from conexion_electrica import Table, Barra, ColumnType, run
from rich import print
import random

from jupyter_dash import JupyterDash
from dataclasses import dataclass
from datetime import datetime, timedelta
import pytz
from working_data import DatoElectrico,TipoBarra
from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd

def size(datafile):
    mb = datafile.stat().st_size/(1024)
    return f"{mb} kB"

def recover(datafile):
    if datafile.exists():
        dbytes = datafile.read_bytes()
        data = pickle.loads(dbytes)
        return data
    else:
        return []

def summarize(dataset):
    new_dataset = []
    first_data = dataset[0]
    new_dataset.append(first_data)
    for data in dataset[1::]:
        if first_data.dt_gen == data.dt_gen:
            first_data += data
        else:
            first_data = data
            new_dataset.append(first_data)
    return new_dataset
    
import itertools
import random

def show(barra, lista, jupyter=False):
    app = Dash(__name__)    
    if jupyter:
        app = JupyterDash(__name__)    
        
    dfiles = random.choices(lista,k=6)
    graphs = []
    labels=dict(dt_gen="Fecha-Tiempo", medida_horaria="Consumo [kWh]")

 
    for i, dfile in enumerate(dfiles):
        data = recover(Path(dfile))
        dataset = {}
        data.sort(key=lambda d: d.tipo.name)
        for key, group in itertools.groupby(data, lambda d: d.tipo):
            dataset[key] = list(group)
            print(key,"#", len(dataset[key]))
        key = random.choice(list(dataset.keys()))

        selection = summarize(dataset[key])
        df = pd.DataFrame([d.to_dict() for d in selection])
        fig = px.line(df, x='dt_gen', y="medida_horaria",
                      title=f'{dfile.parent} | {dfile.stem} -> {key}',
                      labels=labels)
        idk = f"grafica-serie-tiempo-{i}"
        print(idk)
        graph = dcc.Graph(id=idk, figure=fig)
        graphs.append(graph)
        
    app.layout = html.Div(children=[
        html.H1(children=f"Serie Tiempo Energía {barra}"),
        html.Div(children="Aplicación de gráfica con dash-plotly"),
        *graphs
    ])
    
    app.run_server(mode='inline')
    
import sys

if __name__ == "__main__":
    datapath = Path(sys.argv[1])
    name = sys.argv[2]
    if datapath.exists():
        lista = []
        for datafile in datapath.glob("*.pydat"):
            print(size(datafile), datafile)
            lista.append(datafile)
        show(name, lista)
